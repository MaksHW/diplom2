using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Diplom2.Models
{
    public class OrderViewModel
    {
        [Required]
        [Display(Name = "id_baket")]
        public int id_baket { get; set; }
        [Required]
        [Display(Name = "id_user")]
        public int id_user { get; set; }
        [Required]
        [Display(Name = "Adress")]
        public string Adress { get; set; }
        [Required]
        [Display(Name = "Total_price")]
        public int Total_price { get; set; }
        [Required]
        [Display(Name = "Data_in")]
        public DateTime Data_in { get; set; }
    }
}

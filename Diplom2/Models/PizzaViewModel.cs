using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Diplom2.Models
{
    public class PizzaViewModel
    {
        [Required]
        [Display(Name = "id_pizza")]
        public int id_pizza { get; set; }
        [Required]
        [Display(Name = "Price")]
        public int Price { get; set; }
        [Required]
        [Display(Name = "Size")]
        public int Size { get; set; }
        [Required]
        [Display(Name = "Part")]
        public int Part { get; set; }
    }
}

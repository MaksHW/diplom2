using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Diplom2.Models
{
    public class SingViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public int Password { get; set; }
        [Required]
        [Display(Name = "Login")]
        public string Login { get; set; }
    }


}
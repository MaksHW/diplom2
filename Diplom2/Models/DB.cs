﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using Microsoft.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FluentNHibernate;
using System.Data.Entity.ModelConfiguration;

namespace Diplom2.Models
{
    public class DB : Microsoft.EntityFrameworkCore.DbContext
    {

        public class UserContext : Microsoft.EntityFrameworkCore.DbContext
        {
            public Microsoft.EntityFrameworkCore.DbSet<User> User { get; set; }
            public UserContext(DbContextOptions<UserContext> options)
                : base(options)
            {
                Database.EnsureCreated();
            }
        }
        public class BaketContext : Microsoft.EntityFrameworkCore.DbContext
        {
            public Microsoft.EntityFrameworkCore.DbSet<Baket> Baket { get; set; }
            public BaketContext(DbContextOptions<BaketContext> options)
                : base(options)
            {
                Database.EnsureCreated();
            }
        }
        public class BufferContext : Microsoft.EntityFrameworkCore.DbContext
        {
            public Microsoft.EntityFrameworkCore.DbSet<Buffer> Buffer { get; set; }
            public BufferContext(DbContextOptions<BufferContext> options)
                : base(options)
            {
                Database.EnsureCreated();
            }
        }
        public class Baket_addContext : Microsoft.EntityFrameworkCore.DbContext
        {
            public Microsoft.EntityFrameworkCore.DbSet<Baket_add> Baket_add { get; set; }
            public Baket_addContext(DbContextOptions<Baket_addContext> options)
                : base(options)
            {
                Database.EnsureCreated();
            }
        }

        public class Sample_pizzaContext : Microsoft.EntityFrameworkCore.DbContext
        {
            public Microsoft.EntityFrameworkCore.DbSet<Sample_pizza> Sample_pizza { get; set; }
            public Sample_pizzaContext(DbContextOptions<Sample_pizzaContext> options)
                : base(options)
            {
                Database.EnsureCreated();
            }
        }

        public class Sample_IngrContext : Microsoft.EntityFrameworkCore.DbContext
        {
            public Microsoft.EntityFrameworkCore.DbSet<Sample_Ingr> Sample_Ingr { get; set; }
            public Sample_IngrContext(DbContextOptions<Sample_IngrContext> options)
                : base(options)
            {
                Database.EnsureCreated();
            }
        }

        public class OrderContext : Microsoft.EntityFrameworkCore.DbContext
        {
            public Microsoft.EntityFrameworkCore.DbSet<Order> Order { get; set; }
            public OrderContext(DbContextOptions<OrderContext> options)
                : base(options)
            {
                Database.EnsureCreated();
            }
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection("Server=DESKTOP-23FRT8U\\SQLEXPRESS;Database=Pizza;Trusted_connection=True;TrustServerCertificate = True;MultipleActiveResultSets=true");
            }
        }

        public static List<User> GetUserNames()
        {
            var result = Connection.Query<User>("SELECT * FROM [User]").ToList();
            return result;
        }

        
        public static List<Pizza> GetPizzaNames()
        {
            var result = Connection.Query<Pizza>("SELECT * FROM [Pizza]").ToList();
            return result;
        }

        public static List<Pizza> GetPizza(int id)
        {
            var result = Connection.Query<Pizza>($"SELECT * FROM [Pizza] WHERE [id] =('{id}')").ToList();
            return result;
        }

        public static List<Ingredient> GetIngr(int id)
        {
            var buf_id = Connection.Query<Pizza_Ingr>($"SELECT [id_ingr] FROM [Pizza_Ingr] WHERE [id_pizza] =('{id}')").ToList();
            List<Ingredient> result = new List<Ingredient>();
            for (int i = 0; i < buf_id.Count; i++)
            {
                result.Add(Connection.Query<Ingredient>($"SELECT * FROM [Ingredient] WHERE [id] =('{buf_id[i].id_ingr}')").ToList()[0]);
            }
            return result;
        }

        public static int GetIdSample()
        {
            var all = Connection.Query<Sample_pizza>("SELECT * FROM [Sample_pizza]").ToList();
            var result = all[all.Count - 1].id;
            return result;
        }

        public static int GetIdBaket()
        {
            var all = Connection.Query<Baket>("SELECT * FROM [Baket]").ToList();
            var result = all[all.Count - 1].id;
            return result;
        }

        public static double GetBaketPrice(int id)
        {
            var result = Connection.Query<Baket>($"SELECT [Price_baket] FROM [Baket] WHERE [id] =('{id}')").ToList()[0].Price_baket;
            return result;
        }

        public static string GetAuthUserName(string emeil)
        {
            var result = Connection.Query<User>($"SELECT [Name] FROM [User] WHERE [Login] =('{emeil}')").ToList()[0].Name;
            return result;
        }

        public static int GetAuthUserId(string emeil)
        {
            var result = Connection.Query<User>($"SELECT [id] FROM [User] WHERE [Login] =('{emeil}')").ToList()[0].id;
            return result;
        }

        public static List<Addition> GetAdd()
        {
            var result = Connection.Query<Addition>($"SELECT * FROM [Addition]").ToList();
            return result;
        }

        public static List<Sample_pizza> GetSamples(int id)
        {
            var result = Connection.Query<Sample_pizza>($"SELECT * FROM [Sample_pizza] WHERE [id_user] =('{id}')").ToList();
            return result;
        }

        public static List<Baket> GetBakets(int id)
        {
            var result = Connection.Query<Baket>($"SELECT * FROM [Baket] WHERE [id_user] =('{id}')").ToList();
            return result;
        }
        public static double GetSamplesPriceById(int id)
        {
            var result = Connection.Query<Sample_pizza>($"SELECT [Price] FROM [Sample_pizza] WHERE [id] =('{id}')").ToList()[0].Price;
            return result;
        }

        public static string GetPizzaName(int id)
        {
            var result = Connection.Query<Pizza>($"SELECT [Name] FROM [Pizza] WHERE [id] =('{id}')").ToList()[0].Name;
            return result;
        }

        public static int GetIdSamplePizza(int id)
        {
            var result = Connection.Query<Sample_pizza>($"SELECT [id_pizza] FROM [Sample_pizza] WHERE [id] =('{id}')").ToList()[0].id_pizza;
            return result;
        }

        public static int GetBufferIdSample(int id)
        {
            var result = Connection.Query<Buffer>($"SELECT [id_sample] FROM [Buffer] WHERE [id_baket] =('{id}')").ToList()[0].id_sample;
            return result;
        }

        public static User GetUserData(int id)
        {
            var result = Connection.Query<User>($"SELECT * FROM [User] WHERE [id] =('{id}')").ToList()[0];
            return result;
        }

        public static List<Order> GetAllOrder(int id)
        {
            var result = Connection.Query<Order>($"SELECT * FROM [Order] WHERE [id_user] =('{id}')").ToList();
            return result;
        }
        public static List<Baket> GetAllBaket(int id)
        {
            var result = Connection.Query<Baket>($"SELECT * FROM [Baket] WHERE [id_user] =('{id}')").ToList();
            return result;
        }
        public static List<Sample_pizza> GetAllSample(int id)
        {
            var result = Connection.Query<Sample_pizza>($"SELECT * FROM [Sample_pizza] WHERE [id_user] =('{id}')").ToList();
            return result;
        }
        public class User
        {
            public int id { get; set; }
            public string Name { get; set; }
            public int Cash { get; set; }
            public int Password { get; set; }
            public int Admin { get; set; }
            public string Login { get; set; }
        }

        public class Order
        {
            public int id { get; set; }
            public int id_user { get; set; }
            public int id_baket { get; set; }
            public double Total_price { get; set; }
            public DateTime Data_in { get; set; }
            public string Adress { get; set; }
        }

        public class Baket
        {
            public int id { get; set; }
            public int id_user { get; set; }
            public double Price_baket { get; set; }
        }

        public class Pizza
        {
            public int id { get; set; }
            public string Name { get; set; }
            public int Price_pizza { get; set; }
        }
        public class Sample_pizza
        {
            public int id { get; set; }
            public int id_pizza { get; set; }
            public int id_user { get; set; }
            public double Price { get; set; }
            public int Size { get; set; }
            public int Part { get; set; }
        }

        public class Buffer
        {
            public int id_sample { get; set; }
            public int id_baket { get; set; }
            public int Count { get; set; }
            public int id { get; set; }
        }

        public class Pizza_Ingr
        {
            public int id_pizza { get; set; }
            public int id_ingr { get; set; }
        }

        public class Ingredient
        {
            public int id { get; set; }
            public string Name { get; set; }
            public int Price_ingr { get; set; }
        }
        public class Sample_Ingr
        {
            public int id { get; set; }
            public int id_sample { get; set; }
            public int id_ingr { get; set; }
            public int Multilier { get; set; }
        }

        public class Baket_add
        {
            public int id_baket { get; set; }
            public int id_add { get; set; }
            public int Count { get; set; }
            public int id { get; set; }
        }
        public class Addition
        {
            public int id { get; set; }
            public string Name { get; set; }
            public int Price_add { get; set; }
        }
    }
}

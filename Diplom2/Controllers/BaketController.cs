﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Diplom2.Models.DB;

namespace Diplom2.Controllers
{
    public class BaketController : Controller
    {

        private BaketContext db;
        private BufferContext db1;
        private Baket_addContext db2;

        public BaketController(BaketContext context, BufferContext context1, Baket_addContext context2)
        {
            db2 = context2;
            db1 = context1;
            db = context;
        }
        public IActionResult Basket()
        {
            var model =GetAuthUserId(User.Identity.Name);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Save(string b, string count, string sample)
        {
            double total_price = 0;
            var ccount = int.Parse(count);
            var ssample_id = int.Parse(sample);
            total_price = total_price + GetSamplesPriceById(ssample_id);
            total_price = total_price * ccount;
            var adds = GetAdd();
            List<int> addsn = new List<int>();
            Regex regex = new Regex(@"\d*");
            MatchCollection matches = regex.Matches(b);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    if (!(match.Value == "")) { addsn.Add(int.Parse(match.Value)); }
            }

            for (int i = 0; i < addsn.Count; i++)
            {
                var j = adds[i].Price_add * addsn[i];
                total_price = total_price + j;
            }
            var user_id = GetAuthUserId(User.Identity.Name);
            db.Baket.Add(new Baket { id_user = user_id, Price_baket = total_price });
            await db.SaveChangesAsync();
            var bakets = GetIdBaket();
            db1.Buffer.Add(new Diplom2.Models.DB.Buffer { id_baket = bakets, id_sample = ssample_id, Count = ccount });
            for (int i = 0; i < adds.Count; i++)
            {
                db2.Baket_add.Add(new Baket_add { id_baket = bakets, id_add = adds[i].id, Count = addsn[i] });
            }
            await db1.SaveChangesAsync();
            await db2.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public ActionResult Summa(string b, string count, string sample)
        {
            double total_price = 0;
            var ccount = int.Parse(count);
            var ssample_id = int.Parse(sample);
            total_price = total_price + GetSamplesPriceById(ssample_id);
            total_price = total_price * ccount;
            var ingr = GetAdd();
            List<int> ingrn = new List<int>();
            Regex regex = new Regex(@"\d*");
            MatchCollection matches = regex.Matches(b);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    if (!(match.Value == "")) { ingrn.Add(int.Parse(match.Value)); }
            }

            for (int i = 0; i < ingrn.Count; i++)
            {
                var j = ingr[i].Price_add * ingrn[i];
                total_price = total_price + j;
            }
            return PartialView(total_price);
        }
    }
}

﻿using Diplom2.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Diplom2.Models.DB;


namespace Diplom2.Controllers
{
    public class PizzaController : Controller
    {
        private Sample_pizzaContext db;
        private Sample_IngrContext db1;

        public PizzaController(Sample_pizzaContext context, Sample_IngrContext context1)
        {
            db1 = context1;
            db = context;
        }
        [HttpPost]
        public async Task<IActionResult> Save(string size, string part, string b, string id)
        {
            var pizza = GetPizza(int.Parse(id));
            double total_price = pizza[0].Price_pizza;
            var ssize = int.Parse(size);
            if (ssize == 1)
            {
                total_price = total_price / 2;
            }
            else if (ssize == 3)
            {
                total_price = total_price * 2;
            }
            var ppart = int.Parse(part);
            if (ppart == 3)
            {
                total_price = total_price * 0.75;
            }
            else if (ppart == 2)
            {
                total_price = total_price * 0.5;
            }
            else if (ppart == 1)
            {
                total_price = total_price * 0.25;
            }
            var ingr = GetIngr(int.Parse(id));
            List<int> ingrn = new List<int>();
            Regex regex = new Regex(@"\d*");
            MatchCollection matches = regex.Matches(b);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    if (!(match.Value == "")) { ingrn.Add(int.Parse(match.Value)); }
            }

            for (int i = 0; i < ingrn.Count; i++)
            {
                total_price = total_price - ingr[i].Price_ingr;
                var j = ingr[i].Price_ingr * ingrn[i];
                total_price = total_price + j;
            }
            
            var user_id = GetAuthUserId(User.Identity.Name);
            db.Sample_pizza.Add(new Sample_pizza { id_pizza = pizza[0].id, id_user = user_id, Part = ppart, Size = ssize, Price = total_price});
            await db.SaveChangesAsync();
            var samples = GetIdSample();
            for (int i = 0; i < ingr.Count; i++)
            {
                db1.Sample_Ingr.Add(new Sample_Ingr { id_sample = samples, id_ingr = ingr[i].id, Multilier = ingrn[i] });
            }
            await db1.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Pizza(int value)
        {
            ArrayList model = new ArrayList();
            model.Add(value);
            return View(model);
        }
        [HttpPost]
        public ActionResult Summa(string size, string part, string b, string id)
        {
            var pizza = GetPizza(int.Parse(id));
            double total_price = pizza[0].Price_pizza;
            var ssize = int.Parse(size);
            if(ssize == 1)
            {
                total_price = total_price / 2;
            }
            else if(ssize == 3)
            {
                total_price = total_price * 2;
            }
            var ppart = int.Parse(part);
            if(ppart == 3)
            {
                total_price = total_price * 0.75;
            }
            else if(ppart == 2)
            {
                total_price = total_price * 0.5;
            }
            else if (ppart == 1)
            {
                total_price = total_price * 0.25;
            }
            var ingr = GetIngr(int.Parse(id));
            List<int> ingrn = new List<int>();
            Regex regex = new Regex(@"\d*");
            MatchCollection matches = regex.Matches(b);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    if (!(match.Value == "")) { ingrn.Add(int.Parse(match.Value)); }
            }

            for (int i = 0; i < ingrn.Count; i++)
            {
                total_price = total_price - ingr[i].Price_ingr;
                var j = ingr[i].Price_ingr * ingrn[i];
                total_price = total_price + j;
            }
            return PartialView(total_price);
        }
    }
}

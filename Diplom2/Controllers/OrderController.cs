﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Diplom2.Models.DB;

namespace Diplom2.Controllers
{
    public class OrderController : Controller
    {
        private OrderContext db;

        public OrderController(OrderContext context)
        {
            db = context;
        }

        public IActionResult Order()
        {
            var model = GetAuthUserId(User.Identity.Name);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Save(string id_baket, string adress, string data, string time)
        {
            var id_bakets = int.Parse(id_baket);
            time = " " + time;
            DateTime dataa = DateTime.Parse(data+time);
            var user_id = GetAuthUserId(User.Identity.Name);
            db.Order.Add(new Order { id_user = user_id, id_baket = id_bakets, Adress = adress, Data_in = dataa, Total_price = GetBaketPrice(id_bakets) });
            await db.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}

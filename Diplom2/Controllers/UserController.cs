﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using static Diplom2.Models.DB;
using Diplom2.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System;

namespace AuthApp.Controllers
{
    public class UserController : Controller
    {
        private OrderContext dbO;
        private UserContext db;
        private BaketContext dbB;
        private BufferContext dbBuf;
        private Baket_addContext dbAdd;
        private Sample_pizzaContext dbS;
        private Sample_IngrContext dbI;

        public UserController(UserContext context, OrderContext contextO, BaketContext contextB, BufferContext contextBuf, Baket_addContext contextAdd, Sample_pizzaContext contexts, Sample_IngrContext contextI)
        {
            db = context;
            dbO = contextO;
            dbB = contextB;
            dbBuf = contextBuf;
            dbAdd = contextAdd;
            dbS = contexts;
            dbI = contextI;
        }

        [HttpGet]
        public IActionResult Authorization()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Authorization(SingViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await db.User.FirstOrDefaultAsync(u => u.Login == model.Login && u.Password == model.Password);
                if (user != null)
                {
                    await Authenticate(model.Login);

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await db.User.FirstOrDefaultAsync(u => u.Login == model.Login);
                if (user == null)
                {
                    db.User.Add(new User { Login = model.Login, Password = model.Password, Admin = 0, Cash = model.Cash, Name = model.Name });
                    await db.SaveChangesAsync();

                    await Authenticate(model.Login);

                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult UsersItems()
        {
            var model = GetAuthUserId(User.Identity.Name);
            return View(model);
        }

        public IActionResult AdminItems()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> DeletOrder(string id)
        {
            try
            {
                var idd = int.Parse(id);
                Order order = dbO.Order.Single(a => a.id == idd);
                dbO.Remove(order);
                await dbO.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception thrown {0}", e.Message);
            }
            return RedirectToAction("UsersItems", "User");
        }

        [HttpPost]
        public async Task<IActionResult> DeletBaket(string id)
        {
            var idd = int.Parse(id);
            Baket baket = dbB.Baket.Single(a => a.id == idd);
            var buffer = dbBuf.Buffer.Where(b => EF.Property<int>(b, "id_baket") == idd);
            var bak_add = dbAdd.Baket_add.Where(b => EF.Property<int>(b, "id_baket") == idd);
            foreach (var baffer in buffer)
            {
                dbBuf.Remove(baffer);
            }
            foreach (var add in bak_add)
            {
                dbAdd.Remove(add);
            }
            dbB.Remove(baket);
            await dbAdd.SaveChangesAsync();
            await dbBuf.SaveChangesAsync();
            await dbB.SaveChangesAsync();
            return RedirectToAction("UsersItems", "User");
        }

        [HttpPost]
        public async Task<IActionResult> DeletSample(string id)
        {
            try
            {
                var idd = int.Parse(id);
                Sample_pizza sample = dbS.Sample_pizza.Single(a => a.id == idd);
                var ingr = dbI.Sample_Ingr.Where(b => EF.Property<int>(b, "id_sample") == idd);
                foreach (var ing in ingr)
                {
                    dbI.Remove(ing);
                }
                dbS.Remove(sample);
                await dbI.SaveChangesAsync();
                await dbS.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception thrown {0}", e.Message);
            }
            return RedirectToAction("UsersItems", "User");
        }

        private async Task Authenticate(string emeil)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, emeil)

            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }


        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Authorization", "User");
        }
    }
}